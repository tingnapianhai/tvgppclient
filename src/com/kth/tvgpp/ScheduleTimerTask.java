package com.kth.tvgpp;

import java.util.TimerTask;

import com.kth.tvgpp.config.Config;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

public class ScheduleTimerTask extends TimerTask {
	
	//Handler; Message; Looper
	
	Context context;
	String TAG = "ScheduleTimerTask";
	Handler handler;
	Activity ac;
	
	public ScheduleTimerTask() {
		super();
	}
	
	public ScheduleTimerTask(Context context) {
		super();
		this.context = context;
	}
	
	public ScheduleTimerTask(Handler mhandler, Activity ap) {
		super();
		this.handler = mhandler;
		this.ac = ap;
	}
	
	@Override
	public void run() {
		Config.schedule.currentTime++;
		
		if(Config.havingSchedule) {
			Config.currentPlaytingVideoNumber = Config.schedule.getCurrentPlayingVideoNumber(Config.schedule.currentTime);
			Config.schedule.currentTimeMin = Config.schedule.currentTime % Config.schedule.totalTimeMin;
		}
		
		if(Config.havingSchedule && Config.TvGPP_RunningStatus && Config.currentPlaytingVideoNumber_Now!=Config.currentPlaytingVideoNumber) {
			//todo: background layout
			Config.currentPlaytingVideoNumber_Now = Config.currentPlaytingVideoNumber;
			Intent it = new Intent(ac.getApplicationContext(), ClientTest2.class);
			it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
			it.putExtra("layout", Config.ActivityLayout_Playing_Schedule);
			ac.startActivity(it);
			ac.finish();
		}
		
		Message msg = new Message();
		msg.what = 1;
		msg.obj = Config.schedule.currentTime;
		
		Log.v("gppi", Config.currentPlaytingVideoNumber+" "+Config.schedule.currentTimeMin +" "+Config.schedule.currentTime);
		handler.sendMessage(msg);
	}
	
	/*Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        public void run() {
            Toast.makeText(getApplicationContext(), "DISPLAY MESSAGE", Toast.LENGTH_SHORT).show();
            handler.postDelayed(runnable, 5000);
        }
    };*/
	
}
