package com.kth.tvgpp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import com.kth.tvgpp.R;
import com.kth.tvgpp.config.Config;
import com.kth.tvgpp.config.Schedule;
import com.kth.tvgpp.menu.MenuSettingPreferences;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint({ "NewApi", "HandlerLeak" })
public class ClientTest extends Activity {

	private Socket socket;

	//private static final int SERVERPORT = 5000;
	//private static final String SERVER_IP = "130.229.134.14";
	//private Button btn;
	//private Button btnsend;
	//private TextView tview;
	//private EditText et;
	
	private ImageButton ibtn;
	BufferedReader inputinput;
	
	ObjectInputStream is = null;//for schedule
	DataInputStream dataInputStream = null;//for images
	public static final int EDIT_PREFS = 100;
	public static String pref_server_IP = "130.229.135.135";
	public static int pref_server_Port = 5000;
	static int layout = 0;
	
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 1:
					//Log.v("gppi", "handler :-)"+Config.schedule.currentTime);
					setTitle(Config.currentPlaytingVideoNumber + " " + Config.schedule.currentTimeMin + " " +msg.obj);
					break;
				default:setTitle("2");break;
			}
			super.handleMessage(msg);
		}
	};
	Timer scheduleTimer = new Timer();
	ScheduleTimerTask schedule_timerTask;
	//******************************************
	//for timer thread
	private Runnable RunnableTask = new Runnable() {
		public void run() {
			backgroundTask();
		}
	};
	private void backgroundTask() {
		scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = new Timer();
		schedule_timerTask = new ScheduleTimerTask(handler, this);
		scheduleTimer.schedule(schedule_timerTask, 0, 1000);
		Log.v("gppi", "thread");
	}
	Thread timerThread = new Thread(null, RunnableTask, "timerTask_thread");
	//******************************************
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preferenceInitialize();
		layout = getIntent().getIntExtra("layout", 0);//Config.current_Layout_Number
		
		switch(layout) {
			case 0:
				setContentView(R.layout.tvgpp);
				initia_register();
				break;
			case 1:
				setContentView(R.layout.tvgpp);
				initia_registered();
				break;
			default:
				setContentView(R.layout.tvgpp);
				initia_register();
				break;
		}
		
		//add this, otherwise only work on the android-version before android4.0
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	} //end onCreate()
	
	private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			pref_server_IP = p.getString("pref_server_IP", "130.229.134.14");
			pref_server_Port = Integer.parseInt(p.getString("pref_server_Port", "5000"));
			} 
		catch (Exception e) {e.printStackTrace();}
	}
	
	//*******************************************************
	private void initia_register () {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.register);
		
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ibtn.setEnabled(false);
				
				Config.startTime = SystemClock.uptimeMillis();
				timerThread.start();
				
				//check the network connection
				ibtn.setBackgroundResource(R.drawable.loading);
				new Thread(new ClientThread()).start();
			}
		});
		} //end initialize0
	
	private void initia_registered () {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.registered);
		
		//timerThread.start();
		
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				setTitle("pressed after registered");
				
				/*Intent it = new Intent(ClientTest.this, ClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				int number = 1;
				Config.current_Layout_Number = number;
				it.putExtra("layout", number);
				startActivity(it);*/
			}
		});
		
		} //end initialize0
	
	//*******************************************************

	class ClientThread implements Runnable {
		@Override
		public void run() {
			Log.v("gppi", "client socket thread start" );
			Object obj = null;
			
			try {
				InetAddress serverAddr = InetAddress.getByName(pref_server_IP);
				socket = new Socket(serverAddr, pref_server_Port);
				
				//PrintWriter out = new PrintWriter(socket.getOutputStream(),true);
				is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
				
				obj = is.readObject();
				Config.schedule = (Schedule)obj;
	            Log.v("gppi", Config.schedule.getID() + "+++" + Config.schedule.getName());
	            
	            for(int i=0;i<Config.schedule.getImageNumber();i++) {
	            	byte[] decodedString = null;
	            	try { decodedString = Base64.decode(Config.schedule.getImageListContent(i), 0);} 
	            	catch (Exception e) { e.printStackTrace();Log.d("ErrorHere", "" + e);}
	            	Log.v("gppi", ":" + decodedString.length);
	            	saveImg(decodedString, "tvgpp" + i);
	            	}
	            }
			catch (UnknownHostException e) {e.printStackTrace();Log.v("gppi","111");} 
			catch (IOException e) {e.printStackTrace();Log.v("gppi","222");} 
			catch (ClassNotFoundException e) {e.printStackTrace();Log.v("gppi","333");}
			
			try { 
				is.close();
				socket.close();
				Log.v("gppi", "client socket close" );
				
				//to synchronize the time with tvgpp-server
				Config.timeIntervalFromServer = (int) (SystemClock.uptimeMillis()-Config.startTime)/1000;
				Config.startTime = SystemClock.uptimeMillis();
				Config.schedule.currentTime += Config.timeIntervalFromServer;
				//scheduleTimer.cancel();
				//scheduleTimer.purge();
				//schedule_timerTask.cancel();
				//timerThread.interrupt();
				//timerThread = null;
				
				//after loading, to start new layout-background
				Intent it = new Intent(ClientTest.this, ClientTest2.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				Config.current_Layout_Number = 1;
				Config.current_drawable_number = R.drawable.registered;
				it.putExtra("layout", Config.current_Layout_Number);
				startActivity(it);
				
				}
			catch (IOException e) {e.printStackTrace();}
			
            Log.v("gppi", Config.schedule.getID() + "---" + Config.schedule.getName() );
            
            //finish(); //might finish all the activities
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pref:
			Intent intent = new Intent(ClientTest.this, MenuSettingPreferences.class);
			startActivityForResult(intent, EDIT_PREFS);
			return true;
		}
		return false;
	}
	
	//*****************************
	@Override
	protected void onResume() {
		//todo start();
		//timerThread = new Thread(null, RunnableTask, "timerTask_thread");
		//if(timerThread!=null)
		//stopTimerThread ();
		/*Intent it = new Intent(ClientTest.this, ClientTest.class);
		it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
		it.putExtra("layout", Config.current_Layout_Number);
		startActivity(it);*/
		super.onResume();
	}
	
	private void stopTimerThread () {
		scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = null;
		schedule_timerTask.cancel();
		schedule_timerTask = null;
		//Thread.currentThread().interrupt();
		timerThread.interrupt();
		timerThread = null;
	}
	
	@Override
	protected void onPause() {
		//stopTimerThread();
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		//stopTimerThread();
		super.onDestroy();
	}

	@Override
	public void finish() {
		//stopTimerThread();
		super.finish();
	}
	
	//**************************
	public void saveImg (byte[] ops, String imageName) throws IOException {
		// to check if sdcard is exist
		if(android.os.Environment.MEDIA_MOUNTED.endsWith(android.os.Environment.getExternalStorageState())) 
		{
		
		String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
		File dir = new File(sdcard + "/tvgpp");
		dir.mkdir();
		FileOutputStream fos = null;
		try {fos = new FileOutputStream(sdcard+ "/tvgpp/" + imageName + ".jpg" );}
		catch (FileNotFoundException e) {e.printStackTrace();}
		fos.write(ops);
		fos.close();
		}
	}
		
}