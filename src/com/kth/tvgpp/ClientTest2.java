package com.kth.tvgpp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

import com.kth.tvgpp.R;
import com.kth.tvgpp.config.Config;
import com.kth.tvgpp.config.Schedule;
import com.kth.tvgpp.menu.MenuSettingPreferences;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint({ "NewApi", "HandlerLeak" })
public class ClientTest2 extends Activity {

	private Socket socket;
	private ImageButton ibtn;
	BufferedReader inputinput;
	
	ObjectInputStream is = null;//for schedule
	DataInputStream dataInputStream = null;//for images
	public static final int EDIT_PREFS = 100;
	public static String pref_server_IP = "130.229.135.135";
	public static int pref_server_Port = 5000;
	static int layout = 0;
	
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 1:
					//Log.v("gppi", "handler :-)"+Config.schedule.currentTime);
					setTitle(Config.currentPlaytingVideoNumber + " " + Config.schedule.currentTimeMin + " " +msg.obj);
					break;
				default:setTitle("2");break;
			}
			super.handleMessage(msg);
		}
	};
	Timer scheduleTimer = new Timer();
	ScheduleTimerTask schedule_timerTask;
	//******************************************
	//for timer thread
	private Runnable RunnableTask = new Runnable() {
		public void run() {
			backgroundTask();
		}
	};
	private void backgroundTask() {
		scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = new Timer();
		schedule_timerTask = new ScheduleTimerTask(handler, this);
		scheduleTimer.schedule(schedule_timerTask, 0, 1000);
		Log.v("gppi", "thread");
	}
	Thread timerThread = new Thread(null, RunnableTask, "timerTask_thread");
	//******************************************
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preferenceInitialize();
		layout = getIntent().getIntExtra("layout", 0);//Config.current_Layout_Number
		
		Config.havingSchedule = true;
		
		switch(layout) {
			case 1:
				setContentView(R.layout.tvgpp);
				initia_registered();
				break;
			case Config.ActivityLayout_Playing_Schedule:
				setContentView(R.layout.tvgpp);
				initia_playingSchedule();
				break;
			default:
				setContentView(R.layout.tvgpp);
				initia_registered();
				break;
		}
		
		//add this, otherwise only work on the android-version before android4.0
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	} //end onCreate()
	
	private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			pref_server_IP = p.getString("pref_server_IP", "130.229.134.14");
			pref_server_Port = Integer.parseInt(p.getString("pref_server_Port", "5000"));
			} 
		catch (Exception e) {e.printStackTrace();}
	}
	
	//*******************************************************
	
	private void initia_registered () {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.registered);
		//Config.havingSchedule = true;
		
		//timerThread.start();
		
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				setTitle("pressed after registered");
				
				/*Intent it = new Intent(ClientTest.this, ClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				int number = 1;
				Config.current_Layout_Number = number;
				it.putExtra("layout", number);
				startActivity(it);*/
			}
		});
		
		} //end initialize0
	
	private void initia_playingSchedule() {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.lazyload_background);
		
		ibtn.setBackgroundDrawable(Config.getBitmapDrawable(Config.currentPlaytingVideoNumber_Now));
		//********************
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				setTitle("lala");
				
				/*Intent it = new Intent(ClientTest.this, ClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				int number = 1;
				Config.current_Layout_Number = number;
				it.putExtra("layout", number);
				startActivity(it);*/
			}
		});
		
		} //end initialize0
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pref:
			Intent intent = new Intent(ClientTest2.this, MenuSettingPreferences.class);
			startActivityForResult(intent, EDIT_PREFS);
			return true;
		}
		return false;
	}
	
	//*****************************
	@Override
	protected void onResume() {
		//todo start();
		//timerThread = new Thread(null, RunnableTask, "timerTask_thread");
		//if(timerThread!=null)
		//stopTimerThread ();
		/*Intent it = new Intent(ClientTest.this, ClientTest.class);
		it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
		it.putExtra("layout", Config.current_Layout_Number);
		startActivity(it);*/
		Config.TvGPP_RunningStatus = true;
		super.onResume();
	}
	
	private void stopTimerThread () {
		scheduleTimer.cancel();
		scheduleTimer.purge();
		scheduleTimer = null;
		schedule_timerTask.cancel();
		schedule_timerTask = null;
		//Thread.currentThread().interrupt();
		timerThread.interrupt();
		timerThread = null;
	}
	
	@Override
	protected void onPause() {
		//stopTimerThread();
		Config.TvGPP_RunningStatus = false;
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		//stopTimerThread();
		super.onDestroy();
	}

	@Override
	public void finish() {
		//stopTimerThread();
		super.finish();
	}
	
	//**************************
	public void saveImg (byte[] ops, String imageName) throws IOException {
		// to check if sdcard is exist
		if(android.os.Environment.MEDIA_MOUNTED.endsWith(android.os.Environment.getExternalStorageState())) 
		{
		
		String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
		File dir = new File(sdcard + "/tvgpp");
		dir.mkdir();
		FileOutputStream fos = null;
		try {fos = new FileOutputStream(sdcard+ "/tvgpp/" + imageName + ".jpg" );}
		catch (FileNotFoundException e) {e.printStackTrace();}
		fos.write(ops);
		fos.close();
		}
	}
		
}