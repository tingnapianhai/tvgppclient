package com.kth.tvgpp.backup;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.kth.tvgpp.R;
import com.kth.tvgpp.R.id;
import com.kth.tvgpp.R.layout;
import com.kth.tvgpp.config.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint({ "NewApi", "SdCardPath" })
public class ClientTestObj extends Activity {

	private static final String SERVER_IP = "130.229.134.124";
	
	TextView textView;
	ImageView imageView;
	Socket socket = null;
	/*DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null;*/
	
	ObjectInputStream is = null;
	ObjectOutputStream os = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//add this, otherwise only work on the android-version before android4.0
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		textView = (TextView) findViewById(R.id.myTextView2);
		imageView = (ImageView) findViewById(R.id.imageView1);
		
		textView.setText("text---");

		
			try {
				socket = new Socket(SERVER_IP, 5000);
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
				try {
					is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
					os = new ObjectOutputStream(socket.getOutputStream());
				} catch (StreamCorruptedException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			Log.v("gppi", "111");
			Object obj = null;
			Log.v("gppi", "222");
			
			try {
				Log.v("gppi", "777");
				obj = is.readObject();
				Log.v("gppi", "333");
			} catch (OptionalDataException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Log.v("gppi", "444");
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Log.v("gppi", "555");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				Log.v("gppi", "666");
			}
			
            Schedule sch = new Schedule(0, "from client");
            sch = (Schedule)obj;
            if(sch!=null)
            	Log.v("gppi", "not null" + sch.getName());
            else
            	Log.v("gppi", "null");
            textView.setText("+++" + sch.getID() + " " + sch.getName());
            
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
	}
	
	//*****************************************
	//*****************************************

	}