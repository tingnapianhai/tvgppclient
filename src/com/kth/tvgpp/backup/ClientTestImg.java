//working well => transferring image from PC-java-server to phone-terminal; 

package com.kth.tvgpp.backup;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

import com.kth.tvgpp.R;
import com.kth.tvgpp.R.id;
import com.kth.tvgpp.R.layout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

@SuppressLint({ "NewApi", "SdCardPath" })
public class ClientTestImg extends Activity {

	private static final String SERVER_IP = "130.229.171.157";
	ImageView imageView;
	Socket socket = null;
	DataOutputStream dataOutputStream = null;
	DataInputStream dataInputStream = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//add this, otherwise only work on the android-version before android4.0
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		imageView = (ImageView) findViewById(R.id.imageView1);

		try {
			socket = new Socket(SERVER_IP, 5000);
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			dataInputStream = new DataInputStream(socket.getInputStream());
			
			String base64Code = dataInputStream.readUTF();
			byte[] decodedString = null;
			try {decodedString = Base64.decode(base64Code, 0);} 
			catch (Exception e) { e.printStackTrace();Log.d("ErrorHere", "" + e);}
			Log.v("gppi", ":" + decodedString.length);
			saveImg(decodedString, "tai1");
			
			String base64Code2 = dataInputStream.readUTF();
			byte[] decodedString2 = null;
			try {decodedString2 = Base64.decode(base64Code2, 0);} 
			catch (Exception e) { e.printStackTrace();Log.d("ErrorHere", "" + e);}
			Log.v("gppi", ":" + decodedString2.length);
			saveImg(decodedString2, "tai2");
			
			/*byte[] justtest = null;//---
			int length = dataInputStream.read(justtest);//---
			Log.v("gppi", "base64: " + base64Code.length() + " justtest: " + length +"-" + justtest.length);*/
			
			Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0,	decodedString.length);
			
			//Bitmap bitmap2= getHttpBitmap("http://upload.wikimedia.org/wikipedia/commons/c/c0/Kaka_in_Moscow_2007.jpg");
			//saveMyBitmap("kaka1",bitmap2);
			//saveImgFromHttp("kaka2","http://upload.wikimedia.org/wikipedia/commons/c/c0/Kaka_in_Moscow_2007.jpg");
			//saveImg(decodedString, "mmm");
			//saveMyBitmap("trytry",bitmap);
			imageView.setImageBitmap(bitmap);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("Error", "" + e);
		} 
		finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (dataOutputStream != null) {
				try {
					dataOutputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (dataInputStream != null) {
				try {
					dataInputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//*****************************************
	public void saveMyBitmap(String imageName, Bitmap mBitmap){
		String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
		File dir = new File(sdcard + "/tvgpp");
		dir.mkdir();
		
		//File f = new File("/mnt/sdcard/tvgpp" + bitName + ".jpg");
		/*try {f.createNewFile();	} 
		catch (IOException e1) {e1.printStackTrace();}*/
				
		FileOutputStream fOut = null;
		try {fOut = new FileOutputStream(sdcard+ "/tvgpp/" + imageName + ".jpg");} 
		catch (FileNotFoundException e) {e.printStackTrace();}
		mBitmap.compress(Bitmap.CompressFormat.JPEG, TRIM_MEMORY_BACKGROUND, fOut);
		try {fOut.flush();} catch (IOException e) {e.printStackTrace();}
		try {fOut.close();} catch (IOException e) {e.printStackTrace();}
		
		}
	
	public void saveImg (byte[] ops, String imageName) throws IOException {
		// to check if sdcard is exist
		if(android.os.Environment.MEDIA_MOUNTED.endsWith(android.os.Environment.getExternalStorageState())) 
		{
		
		String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
		File dir = new File(sdcard + "/tvgpp");
		dir.mkdir();
		FileOutputStream fos = null;
		try {fos = new FileOutputStream(sdcard+ "/tvgpp/" + imageName + ".jpg" );}
		catch (FileNotFoundException e) {e.printStackTrace();}
		fos.write(ops);
		fos.close();
		
		}
	}
	
	public Bitmap getHttpBitmap(String url)
	{
		Bitmap bitmap = null;
		try
		{
			URL pictureUrl = new URL(url);
			InputStream in = pictureUrl.openStream();
			bitmap = BitmapFactory.decodeStream(in);
			in.close();
		} 
		catch (MalformedURLException e){e.printStackTrace();} 
		catch (IOException e){e.printStackTrace();}
		return bitmap;
	}
	
	public void saveImgFromHttp(String imgName, String url) {
		Bitmap bitmap = getHttpBitmap(url);
		saveMyBitmap(imgName,bitmap);
	}
	//*****************************************

	}