package com.kth.tvgpp.backup;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import com.kth.tvgpp.R;
import com.kth.tvgpp.R.drawable;
import com.kth.tvgpp.R.id;
import com.kth.tvgpp.R.layout;
import com.kth.tvgpp.R.menu;
import com.kth.tvgpp.R.xml;
import com.kth.tvgpp.config.Config;
import com.kth.tvgpp.config.Schedule;
import com.kth.tvgpp.menu.MenuSettingPreferences;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint("NewApi")
public class CopyOfClientTest extends Activity {

	private Socket socket;

	private static final int SERVERPORT = 5000;
	private static final String SERVER_IP = "130.229.134.14";
	private Button btn;
	private Button btnsend;
	private TextView tview;
	private EditText et;
	private ImageButton ibtn;
	BufferedReader inputinput;
	
	ObjectInputStream is = null;//for schedule
	DataInputStream dataInputStream = null;//for images
	public static final int EDIT_PREFS = 100;
	public static String pref_server_IP = "130.229.134.14";
	public static int pref_server_Port = 5000;
	static int layout = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preferenceInitialize();
		layout = getIntent().getIntExtra("layout", 0);//Config.current_Layout_Number
		
		switch(layout) {
			case -1:
				setContentView(R.layout.main);
				initialize();
				break;
			case 0:
				setContentView(R.layout.tvgpp);
				initia_register();
				break;
			case 1:
				setContentView(R.layout.tvgpp);
				initia_registered();
				break;
			default:
				setContentView(R.layout.tvgpp);
				initia_register();
				break;
		}
		
		//add this, otherwise only work on the android-version before android4.0
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	} //end onCreate()
	
	private void preferenceInitialize() {
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
		try {
			pref_server_IP = p.getString("pref_server_IP", "130.229.134.14");
			pref_server_Port = Integer.parseInt(p.getString("pref_server_Port", "5000"));
			} 
		catch (Exception e) {e.printStackTrace();}
	}
	
	//*******************************************************
	private void initia_register () {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.register);
		
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				//check the network connection
				ibtn.setBackgroundResource(R.drawable.loading);
				new Thread(new ClientThread()).start();
			}
		});
		} //end initialize0
	
	private void initia_registered () {
		ibtn = (ImageButton) findViewById(R.id.gpp_imagebutton);
		ibtn.setBackgroundResource(R.drawable.registered);
		
		ibtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				/*Intent it = new Intent(ClientTest.this, ClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				int number = 1;
				Config.current_Layout_Number = number;
				it.putExtra("layout", number);
				startActivity(it);*/
			}
		});
		
		} //end initialize0
	
	private void initialize () {
		btn = (Button) findViewById(R.id.btn);
		btnsend = (Button) findViewById(R.id.btnsend);
		tview = (TextView) findViewById(R.id.myTextView);
		et = (EditText) findViewById(R.id.EditText01);
		
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tview.setText("aaa");
				new Thread(new ClientThread()).start();				
				}
		});
		
		btnsend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent it = new Intent(CopyOfClientTest.this, CopyOfClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				//int number = Integer.parseInt(msg.getArguments().get(0).toString());
				int number = 0;
				Config.current_Layout_Number = number;
				it.putExtra("layout", number);
				startActivity(it);
			}
		});
		
		/*btnsend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				try {
					String str = et.getText().toString();
					PrintWriter out = new PrintWriter(socket.getOutputStream(),true);
					out.println(str);
					}
				catch (UnknownHostException e) {e.printStackTrace();} 
				catch (IOException e) {e.printStackTrace();} 
				catch (Exception e) {e.printStackTrace();}
				}
			});*/
		
		} //end initialize
	//*******************************************************

	class ClientThread implements Runnable {
		
		@Override
		public void run() {
			Log.v("gppi", "client socket thread start" );
			Object obj = null;
			
			try {
				InetAddress serverAddr = InetAddress.getByName(pref_server_IP);
				socket = new Socket(serverAddr, pref_server_Port);
				
				//PrintWriter out = new PrintWriter(socket.getOutputStream(),true);
				is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
				
				obj = is.readObject();
				Config.schedule = (Schedule)obj;
	            Log.v("gppi", Config.schedule.getID() + "+++" + Config.schedule.getName());
	            
	            for(int i=0;i<Config.schedule.getImageNumber();i++) {
	            	byte[] decodedString = null;
	            	try {decodedString = Base64.decode(Config.schedule.getImageListContent(i), 0);} 
	            	catch (Exception e) { e.printStackTrace();Log.d("ErrorHere", "" + e);}
	            	Log.v("gppi", ":" + decodedString.length);
	            	saveImg(decodedString, "tvgpp" + i);
	            	}
	            }
			catch (UnknownHostException e) {e.printStackTrace();Log.v("gppi","111");} 
			catch (IOException e) {e.printStackTrace();Log.v("gppi","222");} 
			catch (ClassNotFoundException e) {e.printStackTrace();Log.v("gppi","333");}
			
			try { 
				is.close();
				socket.close();
				Log.v("gppi", "client socket close" );
				
				//after loading, to start new layout-background
				Intent it = new Intent(CopyOfClientTest.this, CopyOfClientTest.class);
				it.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP);
				Config.current_Layout_Number = 1;
				Config.current_drawable_number = R.drawable.registered;
				it.putExtra("layout", Config.current_Layout_Number);
				startActivity(it);
				
				} 
			catch (IOException e) {e.printStackTrace();}
			
            Log.v("gppi", Config.schedule.getID() + "---" + Config.schedule.getName() );
            
            finish();
		}

		/*@Override
		public void run() {

			try {
				InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
				socket = new Socket(serverAddr, SERVERPORT);
				
				Object obj = null;
				is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
				
				//BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				//String mreceiving = "receiving";
				
				obj = is.readObject();
				
				while( mreceiving!=null && socket.isConnected() ) {
					mreceiving = input.readLine();
					Log.v("gpp", "---" + mreceiving);
					Log.v("gpp", "+++ " + a++);
				}
				
				Log.v("gpp", "no connection "); // if connection is closed
				
			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}*/
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.pref:
			Intent intent = new Intent(CopyOfClientTest.this, MenuSettingPreferences.class);
			startActivityForResult(intent, EDIT_PREFS);
			return true;
		}
		return false;
	}
	
	public void saveImg (byte[] ops, String imageName) throws IOException {
		// to check if sdcard is exist
		if(android.os.Environment.MEDIA_MOUNTED.endsWith(android.os.Environment.getExternalStorageState())) 
		{
		
		String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
		File dir = new File(sdcard + "/tvgpp");
		dir.mkdir();
		FileOutputStream fos = null;
		try {fos = new FileOutputStream(sdcard+ "/tvgpp/" + imageName + ".jpg" );}
		catch (FileNotFoundException e) {e.printStackTrace();}
		fos.write(ops);
		fos.close();
		}
	}
		
}