/*
 * created by Kai 2013-09-16
 * synchronizing between Server and Client; 
 * */

package com.kth.tvgpp.config;

import java.io.Serializable;
import java.util.ArrayList;

public class Schedule implements Serializable {
	
    //public ArrayList <String> imgEncodedString = new ArrayList <String> ();
	//public ArrayList <VideoElement> playList = new ArrayList <VideoElement> ();
    
    private int SCHEDULE_ID = 0; //to specify which schedule-NO. is using in server/client
    private String Location_ID = "348026";
    private String name = "schedule";
    
    private ArrayList <String> imageListContent = new ArrayList <String> ();
    private ArrayList <String> playList_VideoClipsName = new ArrayList <String> ();
    private ArrayList <Integer> playList_VideoClipsTimeInterval = new ArrayList <Integer> ();
    
    public int currentTime = 0;
    public int currentTimeMin = -1;;
    public int totalTimeMin = -1;
    
    public Schedule() {
    	
    }
    	
    public Schedule(String name) {
    	this.name = name;
    	}
    
    public Schedule(int id) {
    	this.SCHEDULE_ID = id;
    	}
    
    public Schedule(int id, String name) {
    	this.SCHEDULE_ID = id;
    	this.name = name;
    	}
    
    public Schedule(int id, String Location_ID, String name) {
    	this.SCHEDULE_ID = id;
    	this.Location_ID = Location_ID;
    	this.name = name;
    }
    
    public int getID () {
    	return SCHEDULE_ID;
    }
    
    public String getName() {
    	return name;
    	}
    
    public void setID(int id) {
    	SCHEDULE_ID = id;
    	}
    
    public void setName(String name) {
    	this.name = name;
    	}
    
    //to show the current video-number in playing
    public int getCurrentPlayingVideoNumber (int current_Time) {
    	current_Time = current_Time % this.totalTimeMin;
    	int size = this.getVideoClipsNumber_InPlayList();
    	int timeSum = 0;
    	if(this.playList_VideoClipsTimeInterval!=null)
    		timeSum = this.playList_VideoClipsTimeInterval.get(0);
    	int index = 0;
    	for(index=0; index<size; index++) {
    		if(current_Time >= timeSum)
    			timeSum += this.playList_VideoClipsTimeInterval.get(index+1);
    		else
    			break;
    	}
    	return index;
    }
    
    public int getVideoClipsNumber_InPlayList () {
    	return this.playList_VideoClipsTimeInterval.size();
    }
    
    //for time-position & total-time
    //************************************************************
    public int getTotalTimeMin () {
    	int time = 0;
    	for(int index=0; index<playList_VideoClipsTimeInterval.size(); index++) {
    		time += playList_VideoClipsTimeInterval.get(index);
    	}
    	this.totalTimeMin = time;
    	return this.totalTimeMin;
    }
    
    public int getTotalTimeMin_direct () {
    	return this.totalTimeMin;
    }
    
    public void setCurrentTime (int time) {
    	this.currentTime = time;
    }
    
    public void setCurrentTimeMin (int currentTime) {
    	this.currentTimeMin = currentTime % this.totalTimeMin;
    }
    
    public int getCurrentTimeMin (int currentTime) {
    	this.currentTimeMin = currentTime % this.totalTimeMin;
    	return this.currentTimeMin;
    }
    
    //for videoElement operation
    //************************************************************
    //to get the videoElement timeInterval
    public int getVideoElement_timeInterval (int index) {
    	return this.playList_VideoClipsTimeInterval.get(index);
    }
    
    //to get the videoElement video-name
    public String getVideoElement_videoName (int index) {
    	return this.playList_VideoClipsName.get(index);
    }
    
    //to add new videoElement (video-name; video-timeInterval)
    public void addVideoElement_InPlayList (String videoName, int timeInterval) {
    	this.playList_VideoClipsName.add(videoName);
    	this.playList_VideoClipsTimeInterval.add( (Integer)timeInterval );
    	this.totalTimeMin += timeInterval;
    }
    
    //for image encoded string operation
    //************************************************************    
    //to save the imaged-encoded's string(base64 format)
    public void addImageListContent (String img) {
    	this.imageListContent.add(img);
    }
    
    //to get the image-encoded's string(base64 format)
    public String getImageListContent (int index) {
    	return this.imageListContent.get(index);
    }
    
    //to get the number of image
    public int getImageNumber () {
    	return this.imageListContent.size();
    }
    //************************************************************
    
    }

	/*//videoElement class
	class VideoElement {
		String videoName = "videoName";
		int videoTimeInterval = 0;
		String backgroundImgEncodedString = "backgroundImgString";
		String MD5 = "md5";
	}*/
