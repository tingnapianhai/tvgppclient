package com.kth.tvgpp.config;

import java.io.Serializable;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;

import com.kth.tvgpp.R;

public class Config implements Serializable {
    /*
     * current_Layout_Number: -1:test ; 0:default layout ; 1:registered successful
     * */
	
	public static boolean havingSchedule = false;
	public static boolean TvGPP_RunningStatus = true;
	
    public static int current_Layout_Number = 0;//activity layout
    public static int current_drawable_number = R.drawable.register;//background layout
    //public static int currentTime = 0;
    public static int currentTimeMin = 0;
    public static int currentPlaytingVideoNumber  = -1;//synchronize with TvGPP-Server
    public static int currentPlaytingVideoNumber_Now = -1;
    
    public static int timeIntervalFromServer = 0;
    public static Schedule schedule = new Schedule(0, "client");
    
    public final int Register = 0;
    public final int Registered = 1;
    public final static int ActivityLayout_Playing_Schedule = 2;
    
    public static Long startTime;
    
    public static BitmapDrawable getBitmapDrawable (int path) {
    	String sdcard = android.os.Environment.getExternalStorageDirectory().toString();
    	BitmapDrawable bd = new BitmapDrawable(BitmapFactory.decodeFile(sdcard + "/tvgpp/"+"tvgpp" + path +".jpg"));
    	return bd;
    }
    
    public Config() {
    	
    }
    
    }
