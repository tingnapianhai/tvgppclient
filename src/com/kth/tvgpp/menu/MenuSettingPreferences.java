package com.kth.tvgpp.menu;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

import org.apache.http.conn.util.InetAddressUtils;

import com.kth.tvgpp.R;
import com.kth.tvgpp.ClientTest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.Preference;
import android.util.Log;

@SuppressLint("NewApi")
public class MenuSettingPreferences extends PreferenceActivity {

	private String TAG = "MenuSettingPreferences";
    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        
        Preference etp = (Preference) findPreference("ip_addr");
        etp.setTitle(getLocalIpAddress());
        
        EditTextPreference server_ip = (EditTextPreference)getPreferenceManager().findPreference("pref_server_IP");
        server_ip.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		ClientTest.pref_server_IP = (String)newValue;
        		Log.d(TAG, "Server IP" );
        		return true;
        		}
        	});//listener
        
        EditTextPreference server_port = (EditTextPreference)getPreferenceManager().findPreference("pref_server_Port");
        server_port.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
        	public boolean onPreferenceChange(Preference preference, Object newValue) {
        		ClientTest.pref_server_Port = Integer.parseInt((String)newValue);
        		Log.d(TAG, "Server Port" );
        		return true;
        		}
        	});//listener
        
    }
    
    public String getLocalIpAddress() {
    	String ipv4 = null;
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();){
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(ipv4 = inetAddress.getHostAddress())) {

                        String ip = inetAddress.getHostAddress().toString();
                        return ipv4;
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }
    
/*	private static int prefs=R.xml.preferences;
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        } catch (NoSuchMethodException e) { //Api < 11
            AddResourceApiLessThan11();
        }
    }
    protected void AddResourceApiLessThan11()
    {
        addPreferencesFromResource(prefs);
    }

    protected void AddResourceApi11AndGreater()
    {
        getFragmentManager().beginTransaction().replace(android.R.id.content, new PF()).commit();
    }
    public static class PF extends PreferenceFragment
    {       
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(SettingPreferences.prefs); //outer class private members seem to be visible for inner class, and making it static made things so much easier
        }
    }*/

}
